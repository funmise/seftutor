from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.utils import timezone
from django.contrib import admin
from django.forms import CheckboxSelectMultiple
from django.db.models import Q
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.
class User(AbstractUser):
	is_teacher = models.BooleanField('Teacher Status', default = False)
	email = models.EmailField(blank =True)
	flogin = models.BooleanField(default = True) #first_login, but first_login is too long, 
	secret_question = models.CharField(max_length=50)
	secret_answer = models.CharField(max_length=10)



class Classe(models.Model):
	Year = models.IntegerField()

	def __str__(self):
		return str(self.Year)
	
class Subject(models.Model):
	name = models.CharField(max_length=50)
	Class = models.ForeignKey(Classe, on_delete = models.CASCADE)
	

	class Meta:
		ordering= ['name']
	
	def __str__(self):
		ans = self.name+ " "+ str(self.Class)
		return ans 
	
class Teacher(models.Model):
	user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
	Subject7  = models.ManyToManyField(Subject, related_name='subjects_7', blank = True)
	Subject8 = models.ManyToManyField(Subject,related_name='subjects_8', blank = True)
	Subject9 = models.ManyToManyField(Subject, related_name='subjects_9', blank = True)
	Subject10 = models.ManyToManyField(Subject, related_name='subjects_10', blank = True)
	Subject11 = models.ManyToManyField(Subject, related_name='subjects_11', blank =True)
	Subject12 = models.ManyToManyField(Subject, related_name = 'subjects_12', blank  =True)
	

	
	def __str__(self):
		return str (self.user)
	
class Student(models.Model):
	user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
	Class = models.ForeignKey(Classe, on_delete = models.CASCADE,null=True)
	Arm = models.CharField(max_length=1)
	Subject = models.ManyToManyField(Subject,blank=True)
	
	
	
	def __str__(self):
		return str (self.user)

class Lesson(models.Model):
	name  = models.CharField(max_length=150)
	description = RichTextUploadingField(max_length=100000,config_name='special',external_plugin_resources=[('youtube','/static/accounts/ckeditor_plugins/youtube/youtube/','plugin.js',)],blank=True ) 
	Teacher = models.ForeignKey(Teacher,on_delete=models.CASCADE)
	document = models.FileField(blank=True,max_length=100000,upload_to='Lessons')
	subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now=True)
	posted_A = models.BooleanField(default = False) 
	posted_B = models.BooleanField(default = False) 
	posted_C = models.BooleanField(default = False)	
	def __str__(self):
		return str (self.name)

	class Meta:
		ordering= ['-date']

	def get_absolute_url(self):
		return '/home/Class/'+str(self.subject.Class)+'/'+self.subject.name +'/'+self.name+'/'

class Contact(models.Model):
    name  = models.CharField(max_length=150)
    email = models.EmailField(max_length=70)
    subject = models.CharField(max_length=11)
    message = models.TextField() 
    
    def __str__(self):
    	return str(self.name)

class PastQuestion(models.Model):
	name = models.CharField(max_length=10, default='PQ')
	subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
	Set = models.IntegerField( null = True)
	Type = models.CharField(max_length=10) #The type of the PastQuestion 
	Exam_Type = models.CharField(max_length=10) #CA 1, CA 2 or other wise
	Term = models.CharField(max_length=10)
	document = models.FileField(upload_to='Past Questions')#remember to do the multiple file fields
	date = models.DateField(auto_now=True)

	class Meta:
		ordering = ['-date']

	def __str__(self):
		return str(self.name)	
	
class Email(models.Model):
	first_name = models.CharField(max_length =20,blank=True)
	last_name = models.CharField(max_length = 20,blank=True)
	email = models.EmailField(blank=True)
		
	def __str__(self):
		return self.email
class MyModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

	
	
@receiver(post_save, sender=User)
def create_or_update_teacher_or_user(sender, instance, created, **kwargs):
	
   if created and instance.is_teacher==True:
       Teacher.objects.create(user=instance)
       instance.teacher.save()
       
   elif created :
           Student.objects.create(user=instance)
           instance.student.save()

@receiver(post_save, sender=Email)
def email_create(sender, instance, created, **kwargs):
	if created:
#		instance.email = instance.first_name+'.'+instance.last_name+'@supremeeducation.com'
		instance.first_name= instance.email.split('.')[0]
		a=instance.email.split('.')[1]
		instance.last_name=a.split('@')[0]
		instance.save()           
			
#@receiver(post_save, sender=Student)
#def create_or_update_Student(sender, instance, created, **kwargs):
#	
#    if created :
#        student = instance
#        #stud_Class = Classe.objects.get(Year = student.Class)
#        subjects = Subject.objects.filter(Q(Class = student.Class) & Q(compulsory=True))
#        for subject in subjects:
#        	student.Subject.add(subject)
#        student.save()
#   #     teacher.save()
#		
#    #elif created and instance.is_teacher == False:
#     #       student = Student.objects.create(user=instance)
#      #      student.save()
			


            
            
   
	
