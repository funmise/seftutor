from django.shortcuts import render, redirect
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages 
from django.contrib.auth.decorators import login_required
from accounts.models import Teacher,Student,Subject,User,Lesson,Contact,PastQuestion,Email
from accounts.models import Classe as Classes
from accounts.forms import EditForm
#from django.contrib import messages 
from accounts.forms import *
from django.db.models import Q
from django.utils import timezone
#from Discuss.models import Post, Topic
#from django.contrib.auth.decorators import user_passes_test
#from accounts.functions import check_is_teacher
#from django.views.generic.edit import UpdateView
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.views.generic.edit import UpdateView
from django.contrib.auth import login, authenticate

# Create your views here.
def home(request):
	if request.user.flogin:
		if request.user.is_teacher:
			return redirect('/home/teacher_update/')
		else:
			return redirect('/home/student_update/')
	else:	
		args = {'user': request.user}
		return render(request,'accounts/index.html',args)


def StudentUpdate(request):
	if request.method == 'POST':
		form = StudentUpdateForm(request.POST)
		if form.is_valid():
			if request.user.is_teacher == False:
				user = User.objects.get(username=request.user.username)
				user.username = form.cleaned_data['username']
				user.email = form.cleaned_data['email']
				user.secret_question = form.cleaned_data['secret_question']
				user.secret_answer = form.cleaned_data['secret_answer']
				user.set_password(form.cleaned_data['password1'])
				user.email = form.cleaned_data['email']
				user.flogin = False
				user.save()
				raw_password = form.cleaned_data['password1']
				logged_user= authenticate(username = user.username, password=raw_password)
				login(request,logged_user)
				return redirect('/home/')
			
		else:

			return render(request, 'accounts/Student_Update_page.html', {'form':form})
	else:
		form = StudentUpdateForm()
		return render(request, 'accounts/Student_Update_page.html',{'form':form})

def TeacherUpdate(request):
	if request.method == 'POST':
		form = TeacherUpdateForm(request.POST)
		if form.is_valid():
			user = User.objects.get(username=request.user.username)
			user.username = form.cleaned_data['username']
			user.secret_question = form.cleaned_data['secret_question']
			user.secret_answer = form.cleaned_data['secret_answer']
			user.set_password(form.cleaned_data['password1']) 
			user.email = form.cleaned_data['email']
			user.flogin = False
			user.save()
			raw_password = form.cleaned_data['password1']
			logged_user= authenticate(username = user.username, password=raw_password)
			login(request,logged_user)
			return redirect('/home/Choose_Subjects/')
		else:
			return render(request, 'accounts/Teacher_Update_page.html',{'form':form})
	else:
		form = TeacherUpdateForm()
		return render(request, 'accounts/Teacher_Update_page.html',{'form':form})		     
#	if request.method == 'POST':
#		if request.user.is_teacher == True:
#			form = TeacherChangeForm(request.POST, request.FILES)
#			if form.is_valid():
#				teacher = Teacher.objects.get(user = request.user)
#				teacher.image = form.cleaned_data['image']
#				teacher.save()
#				return redirect('/home')
#		else:
#			form = StudentChangeForm(request.POST, request.FILES)
#			if form.is_valid():
#				student = Student.objects.get(user = request.user)
#				student.image = form.cleaned_data['image']
#				student.save()
#				return redirect('/home')					
#	else:
#		#group_invites = GroupInvite.objects.filter(Q(new = True) & Q(To = request.user))#get the newset group invites
#		if request.user.is_teacher == True:
#			form = TeacherChangeForm(instance = request.user.teacher)
#			args = {
#				#'group_invites':group_invites, 
#					'form':form}
#			return render(request, 'accounts/index.html',args)
#		else:
#			form = StudentChangeForm(instance = request.user.student)
#			args = {
#				'form':form,
#			}
#			return render(request, 'accounts/index.html',args)

		
def student_register(request):
	if request.method == 'POST':
		form = StudentRegistrationForm(request.POST)
		if form.is_valid():
			email_exists = Email.objects.filter(email = form.cleaned_data.get('email')) #stuff relevant to the email
			if email_exists: #Check if the email is in our database already
				info = form.save(commit=False)
				new_user = User.objects.create(username = info.username,
					                           password = info.password,
					                           email = info.email,
					                           first_name = email_exists[0].first_name,
					                           last_name = email_exists[0].last_name) # The new user
			                                   #creating the student model for the user
				student = Student.objects.get(user = new_user) #Get the user created
				student.Class = form.cleaned_data.get('Class')
				student.Arm = form.cleaned_data.get('Arm')
				student.save()
				#authenticating and login in the user
				username = form.cleaned_data.get('username')
				raw_password = form.cleaned_data.get('password1')
				user = authenticate(username = username, password = raw_password)
				login(request, user)
				return redirect('/home/')
			    
			    
			else: #show the errors
				error = 'Enter a valid supremeeducation email '
				args = {'form':form, 'error':error}

				return render(request,'accounts/register.html', args)

		
	else:
		form = StudentRegistrationForm()
		
	return render(request,'accounts/register.html', {'form':form})

def teacher_register(request):
	if request.method == 'POST':
		form = TeacherRegistrationForm(request.POST)
		if form.is_valid():
			email_exists = Email.objects.filter(email = form.cleaned_data.get('email'))
			if email_exists:

			    info = form.save(commit=False)
			    User.objects.create(username = info.username,
										password = info.password,
										email = info.email,
										is_teacher = True,
								   first_name = email_exists[0].first_name,
								   last_name = email_exists[0].last_name) # The new user
			    #creating the teacher model for the user

			    #authenticating and login in the user
			    username = form.cleaned_data.get('username')
			    raw_password = form.cleaned_data.get('password1')
			    user = authenticate(username = username, password = raw_password)
			    login(request, user)
			    return redirect('/home/Choose_Subjects/')
			else:
				error = 'Enter a valid supremeeducation email'
				args = {'form':form,'error':error}
				return render(request, 'accounts/teacher_register.html',args)
	else:
		form = TeacherRegistrationForm()
		
	return render(request, 'accounts/teacher_register.html',{'form':form})


def change_password(request):
    if request.method=='POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.info(request, 'Your password has been changed successfully!')
            return redirect('accounts:home')
            
        else:
            
            args = {'form': form}
            return render(request, 'accounts/change_password.html',args)
            
            
        
     
    
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
        return render(request, 'accounts/change_password.html',args)		

def ClassView(request,year):
    if request.method == 'POST':
        form = EditForm(request.POST)
        if form.is_valid():
            info = form.save(commit=False) #get the information from the form
            student  = Student.objects.get(user = request.user) #get the student object
            
            for subject in student.Subject.all():
                if(subject.Class == Classes.objects.get(Year = year) ):
                    student.Subject.remove(subject) #remove all the existing subjects in the students subject set except the compulsory ones
                                                     #and the ones that are not in this class

            for subject in form.cleaned_data['Subject']:
                student.Subject.add(subject) #add the new subjects

            student.save()#save the student subjects
            return redirect('/home/Class/'+str(year)+'/')
    else:
        student = Student.objects.get(user = request.user)
        Class = Classes.objects.get(Year = year)
        subjects = student.Subject.filter(Class = Class)
        form = EditForm(instance=student)
        
        form.fields['Subject'].queryset =  Subject.objects.filter(Q(Class = Class))
        Lesson = []
    
        #Lists containing Lists, don't get confused, it had me for a long time

        for subject in subjects:
            Lesson.append(subject.lesson_set.all()[:5])

        args = {'Class':Class,'subjects':subjects,'Lesson':Lesson,'form':form}
        return render(request,'accounts/Class.html', args)    
     

def contact(request):

    if request.method == 'GET':
        form =  ContactForm()
        args = {'form': form}
        return render(request, 'accounts/contact.html',args)

    elif request.method == 'POST':
        username = User.objects.get(username = request.user)
        form = ContactForm(request.POST)
        if form.is_valid():
            info = form.save(commit=False)
            info.name = username
            email = form.cleaned_data.get('email'),
            subject = form.cleaned_data.get('subject'),
            message = form.cleaned_data.get('message'),
            wmessage = request.POST.get('message', '')
            from_email = request.POST.get('email', '')
            form.save()
            messages.info(request, 'Your message has been sent successfully!')
            return redirect('accounts:home')

            
def LessonView(request,year,subject_name, lesson_name):
    Class = Classes.objects.get(Year=year)
    subjects = Class.subject_set.get(name=subject_name)
    lesson = subjects.lesson_set.get(name=lesson_name)
												
    args = {'lesson':lesson}

    return render(request, 'accounts/lesson_page.html', args)    

        
def search(request):
    query = request.GET.get("q")
    #This is only temporary
    if query:
        Lesson_results = list(Lesson.objects.filter(Q(name__icontains = query) | Q(description__icontains = query) | Q(document__icontains = query)))
        pq_results = list(PastQuestion.objects.filter( Q(name__icontains = query) | Q(Exam_Type__icontains = query) | Q(Set__icontains = query) | Q(Term__icontains = query) | Q(Type__icontains = query) ))
        results = Lesson_results + pq_results	
        paginator = Paginator(results, 10)
        page = request.GET.get('page')
        res = paginator.get_page(page)
        args = {'res':res, 'query':query}
        return render(request, 'accounts/results.html',args)
    else:
        return redirect('accounts:home')

def SeeAll(request,year,subject_name):
    Class_ = Classes.objects.get(Year = year)
    subject = Subject.objects.get(Q(Class = Class_) & Q(name = subject_name))
    sub = subject.name
    result = subject.lesson_set.all()
    paginator = Paginator(result,10)
    page = request.GET.get('page')
    res = paginator.get_page(page)
    args = {'result':result,'subject':subject,'sub':sub,'res':res,}
    return render(request,'accounts/SeeAll.html',args)

def addContent(request):
	if request.method == 'POST':
		User = Teacher.objects.get(user = request.user) #The teacher adding the lesson
		form = addContentForm(request.POST,request.FILES)
		if form.is_valid():
			info = form.save(commit=False)
			info.Teacher = User
			info.date  = timezone.now()
			info.save()
			return redirect('/home/Lessons/')
		else:
			print("not valid")
	else:
		form = addContentForm()
		teacher = Teacher.objects.get(user = request.user)
		sub = teacher.Subject7.all() | teacher.Subject8.all() | teacher.Subject9.all() | teacher.Subject10.all() | teacher.Subject11.all() | teacher.Subject12.all() 
		form.fields['subject'].queryset = sub#the list of all the subjects that the students do
		lessons = Lesson.objects.filter(Teacher = teacher) #get a list of the lessons the teacher has written
		args = {'form':form, 'lessons':lessons, 'teacher':teacher}
		return render(request, 'accounts/create_lesson.html', args)


	
def ManageLessonView(request, pk):
	if request.method == 'POST':
		lesson = Lesson.objects.get(pk = pk)
		form = addContentForm(request.POST,request.FILES)
		if form.is_valid():
			lesson.name = form.cleaned_data['name']
			lesson.description = form.cleaned_data['description']
			lesson.subject = form.cleaned_data['subject']
			lesson.document = form.cleaned_data['document']
			lesson.posted_A = form.cleaned_data['posted_A']
			lesson.posted_B = form.cleaned_data['posted_B']
			lesson.posted_C = form.cleaned_data['posted_C']
			lesson.save()
			return redirect('/home/Lessons/')
		else:
			print("not valid")
	else:
		teacher = Teacher.objects.get(user = request.user)
		lesson = Lesson.objects.get(pk = pk)
		form = addContentForm(instance = lesson)
		sub = teacher.Subject7.all() | teacher.Subject8.all() | teacher.Subject9.all() | teacher.Subject10.all() | teacher.Subject11.all() | teacher.Subject12.all() 
		form.fields['subject'].queryset = sub#the list of all the subjects that the students do
		
		args = {'form':form}
		return render(request, 'accounts/LessonEdit.html', args)

def teacher(request):
	User = Teacher.objects.get(user = request.user)
	teacher = Teacher.objects.get(user = request.user)
	lessons =list(Lesson.objects.filter(Teacher = teacher))
	paginator = Paginator(lessons,10)
	page = request.GET.get('page')
	les = paginator.get_page(page)
	args = {'lessons':lessons, 'teacher':teacher,'les':les}
	return render(request, 'accounts/teacher.html', args)

def ArchiveView(request, PQ_Type):
	Type = PQ_Type #Use the past question type to separate the different past questions
	PQ_set = PastQuestion.objects.filter(Type = Type) #Create a set of past questions with the same type
	PQ = []
	PQ_Sete = PastQuestion.objects.all()
	counter =0
	for i in PQ_Sete:
		if  i.Type == 'SAT':
			PQ.append(i)
			
	paginator = Paginator(PQ, 10)
	page = request.GET.get('page')
	les = paginator.get_page(page)
	args = {'type':Type,'PQ':PQ,'les':les}
	return render(request, 'accounts/archive.html', args)

def add_To_Archive(request):
	if request.method == 'POST':
		form = AddToArchiveForm(request.POST, request.FILES)
		if form.is_valid():
			for f in request.FILES.getlist('document'):
				PastQuestion.objects.create(
					name= form.cleaned_data['name'], 
					subject = form.cleaned_data['subject'],
					Set = form.cleaned_data['Set'],
					Type = form.cleaned_data['Type'],
					Exam_Type = form.cleaned_data['Exam_Type'],
                    Term = form.cleaned_data['Term'],
					document = f,)
				
		else:
			error = 'Please fill out all fields'
			args = {'form':form,'error':error}
			return render(request,'accounts/Add_To_Archive.html', args)
		return redirect('/home/archive/SEF/')
	else:
		form = AddToArchiveForm()
		args= {'form':form}
		return render(request, 'accounts/Add_To_Archive.html', args)
	
def SEFArchive(request, year):
	PQ = []
	PQ_Set = PastQuestion.objects.all()
	counter =0
	for i in PQ_Set:
		if i.subject.Class.Year == year and i.Type == 'SEF':
			PQ.append(i)
	
	args = {'PQ':PQ,}
	return render(request, 'accounts/SEFarchive.html', args)

def SEFArchiveSUB(request, year,subject_name):
	PQ = []
	PQ_Set = PastQuestion.objects.all()
	counter =0
	for i in PQ_Set:
		if i.subject.Class.Year == year and i.Type == 'SEF' and i.subject.name == subject_name :
			PQ.append(i)
	
	args = {'PQ':PQ,}
	return render(request, 'accounts/SEFarchiveSUB.html', args)

def SEFArchiveSUBSET(request, year,subject_name,Set):
	sub = subject_name
	PQ = []
	PQ_Set = PastQuestion.objects.all()
	counter =0
	for i in PQ_Set:
		if i.subject.Class.Year == year and i.Type == 'SEF' and i.subject.name == subject_name and i.Set == Set :
			PQ.append(i)

	paginator = Paginator(PQ, 10)
	page = request.GET.get('page')
	res = paginator.get_page(page)
	args = {'PQ':PQ,'sub':sub,'res':res}
	return render(request, 'accounts/SEFarchiveSUBSET.html', args)
	
def WAECArchive(request,subject_name):
	sub = subject_name
	PQ = []
	PQ_Set = PastQuestion.objects.all()
	counter =0
	for i in PQ_Set:
		if  i.Type == 'WAEC' and i.subject.name == subject_name  :
			PQ.append(i)

	paginator = Paginator(PQ, 10)
	page = request.GET.get('page')
	res = paginator.get_page(page)
	args = {'PQ':PQ,'sub':sub,'res':res}
	return render(request, 'accounts/WAECarchive.html', args)
	
def Choose_Subjects(request):
	if request.method == 'POST':
		form = ChooseSubjectsForm(request.POST)
		if form.is_valid():
			teacher = Teacher.objects.get(user = request.user)

			for subject in teacher.Subject7.all():
				teacher.Subject7.remove(subject)

			for subject in teacher.Subject8.all():
				teacher.Subject8.remove(subject)

			for subject in teacher.Subject9.all():
				teacher.Subject9.remove(subject)

			for subject in teacher.Subject10.all():
				teacher.Subject10.remove(subject)

			for subject in teacher.Subject11.all():
				teacher.Subject11.remove(subject)

			for subject in teacher.Subject12.all():
				teacher.Subject12.remove(subject)

			for subject in form.cleaned_data['Subject7']:
				teacher.Subject7.add(subject)

			for subject in form.cleaned_data['Subject8']:
				teacher.Subject8.add(subject)

			for subject in form.cleaned_data['Subject9']:
				teacher.Subject9.add(subject)

			for subject in form.cleaned_data['Subject10']:
				teacher.Subject10.add(subject)

			for subject in form.cleaned_data['Subject11']:
				teacher.Subject11.add(subject)

			for subject in form.cleaned_data['Subject12']:
				teacher.Subject12.add(subject)

			teacher.save()	
			return redirect('/home/Lessons/')

	else:
		teacher = Teacher.objects.get(user = request.user)
		form = ChooseSubjectsForm(instance = teacher)
		
		Class12 = Classes.objects.get(Year = 12)
		Class11 = Classes.objects.get(Year = 11)
		Class10 = Classes.objects.get(Year = 10)
		Class9 = Classes.objects.get(Year = 9)
		Class8 = Classes.objects.get(Year = 8)
		Class7 = Classes.objects.get(Year = 7)
		
		form.fields['Subject7'].queryset = Subject.objects.filter(Class = Class7)
		form.fields['Subject8'].queryset = Subject.objects.filter(Class = Class8)
		form.fields['Subject9'].queryset = Subject.objects.filter(Class = Class9)
		form.fields['Subject10'].queryset = Subject.objects.filter(Class = Class10)
		form.fields['Subject11'].queryset = Subject.objects.filter(Class = Class11)
		form.fields['Subject12'].queryset = Subject.objects.filter(Class = Class12)
		args = {'form':form}
		return render(request,'accounts/Choose_Subjects.html', args)
    
def LessonDelete(request, pk):
	if request.method == 'POST':
		lesson = Lesson.objects.get(pk = pk)
		lesson.delete()
		return redirect('/home/Lessons/')    

def get_username(request):
	if request.method == 'POST':
		form = GetUsernameForm(request.POST)
		if form.is_valid():
			user = User.objects.get(username = form.cleaned_data['username'])
			return redirect('/home/reset_password/'+str(user.pk)+'/reset/')
		else:
			return render(request, 'accounts/GetUsernamePage.html', {'form':form})
	else:
		form = GetUsernameForm()

		return render(request, 'accounts/GetUsernamePage.html', {'form':form})


def reset_password(request, pk):
	if request.method == 'POST':
		form = CustomPasswordChangeForm(request.POST)
		if form.is_valid():
			secret_answer = form.cleaned_data['secret_answer']
			user = User.objects.get(pk = pk)
			if(secret_answer == user.secret_answer):
				user.set_password(form.cleaned_data['password1'])
				user.save()
				raw_password = form.cleaned_data['password1']
				logged_user = authenticate(username = user.username, password = raw_password)
				login(request, logged_user)
				return redirect('/home/')
			else:
				return render(request, 'accounts/ResetPassword.html',{'form':form})
	else:
		form = CustomPasswordChangeForm()
		user = User.objects.get(pk = pk)
		return render(request, 'accounts/ResetPassword.html', {'form':form,'user':user})