from django import forms
from django.forms import ModelForm
from accounts.models import Lesson,Student,Contact,PastQuestion, Teacher,User,Subject
from django.forms.models import ModelMultipleChoiceField
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.forms import UserCreationForm
from accounts.models import Classe as Classes

class addContentForm(ModelForm):
	class Meta:
		model = Lesson
		
		fields = ['name','subject','description','document','posted_A','posted_B','posted_C']

		
class CustomModelMultipleChoiceField(ModelMultipleChoiceField):
	def label_from_instance(self,obj):
		return "%s" % (obj.name)
	
	
class EditForm(ModelForm):
	class Meta:
		model = Student
		fields = ['Subject']
		field_classes ={
			'Subject':CustomModelMultipleChoiceField,
		}

		widgets = {
		'Subject': forms.CheckboxSelectMultiple()
		}

class ContactForm(ModelForm):

    class Meta:
    	model= Contact
    	fields =['subject','email','message']
		
class AddToArchiveForm(ModelForm):
	name = forms.CharField(required=True)
	class Meta:
		model = PastQuestion
		fields = '__all__'
		#widgets = {
		#'document': forms.FileInput(attrs={'id':'document'})
		#}

		

class StudentRegistrationForm(UserCreationForm):
	email = forms.EmailField(required=True)
	Class = forms.ModelChoiceField(required = True, queryset = Classes.objects.all())
	Arm = forms.CharField(max_length=1)
	class Meta:
		model = User
		fields = ['username', 'email','password1','password2', 'Class','Arm']


class TeacherRegistrationForm(UserCreationForm):
	email = forms.EmailField(required=True)
	class Meta:
		model = User
		fields = ['username', 'email','password1','password2']
class StudentUpdateForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username','email','password1','password2','secret_question','secret_answer']


class TeacherUpdateForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username','email','password1','password2','secret_question','secret_answer']

class GetUsernameForm(forms.Form):
	username = forms.CharField(max_length  = 10)

		

class CustomPasswordChangeForm(UserCreationForm):
	class Meta:
		model = User	
		fields = ['secret_answer','password1','password2']	

class ChooseSubjectsForm(ModelForm):
	class Meta:
		model = Teacher
		fields = ['Subject7', 'Subject8','Subject9','Subject10','Subject11','Subject12']
		field_classes={
		'Subject7':CustomModelMultipleChoiceField,
		'Subject8':CustomModelMultipleChoiceField,
		'Subject9':CustomModelMultipleChoiceField,
		'Subject10':CustomModelMultipleChoiceField,
		'Subject11':CustomModelMultipleChoiceField,
		'Subject12':CustomModelMultipleChoiceField
		}
		widgets = {
		'Subject7': forms.CheckboxSelectMultiple(),
		'Subject8': forms.CheckboxSelectMultiple(),
		'Subject9': forms.CheckboxSelectMultiple(),
		'Subject10': forms.CheckboxSelectMultiple(),
		'Subject11': forms.CheckboxSelectMultiple(),
		'Subject12': forms.CheckboxSelectMultiple()
		}
		labels = {
		'Subject7': 'Year 7',
		'Subject8': 'Year 8',
		'Subject9': 'Year 9',
		'Subject10': 'Year 10',
		'Subject11': 'Year 11',
		'Subject12': 'Year 12',	
		
		}