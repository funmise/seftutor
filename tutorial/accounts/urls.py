from django.urls import re_path
from . import view
from django.contrib.auth.views import login,logout,password_reset,password_reset_done,password_reset_confirm,password_reset_complete
from django.urls import path

app_name="accounts"

urlpatterns = [
	path('',view.home, name='home'),
	path('login/',login,{'template_name': 'accounts/login.html'},name='login'),
	path('logout/',logout,{'template_name': 'accounts/logout.html'},name='logout'), 
	
	path('change_password/',view.change_password,name='change_password'),
	path('reset_password/',view.get_username, name='reset_password'),
	path('reset_password/<int:pk>/reset/',view.reset_password),
	# path('reset_password/',password_reset,{'template_name': 'accounts/reset_password.html', 'post_reset_redirect': 'accounts:password_reset_done', 'email_template_name':'accounts/reset_password_email.html'},name='reset_password'),
	
	# path('reset_password/done/',password_reset_done, {'template_name': 'accounts/reset_password_done.html'},name='password_reset_done'),
	# re_path(r'^reset_password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',password_reset_confirm,{'template_name': 'accounts/reset_password_confirm.html','post_reset_redirect':'accounts:password_reset_complete'},name='password_reset_confirm'),
	# path('reset_password/complete/',password_reset_complete,{'template_name': 'accounts/reset_password_complete.html'},name='password_reset_complete'),

	path('Class/<int:year>/',view.ClassView, name='ClassView'),
	path('Class/<int:year>/<str:subject_name>/See All/',view.SeeAll,name = 'SeeAll'),
	path('Class/<int:year>/<str:subject_name>/<str:lesson_name>/',view.LessonView, name='LessonView'),
    
    path('Lessons/',view.teacher, name='Teachers'),
	path('Lessons/<int:pk>/edit/', view.ManageLessonView, name='Manage Lesson'),
	path('Lessons/add/', view.addContent, name='Create_lesson'),
    path('Lessons/<int:pk>/delete/', view.LessonDelete, name='Delete Lesson'),

	path('archive/add/', view.add_To_Archive, name='add_to_archive'),
	path('archive/SEF/<int:year>/', view.SEFArchive, name='SEFarchive'),
	path('archive/SEF/<int:year>/<str:subject_name>/', view.SEFArchiveSUB, name='SEFarchiveSUB'),
	path('archive/SEF/<int:year>/<str:subject_name>/<int:Set>/', view.SEFArchiveSUBSET, name='SEFarchiveSUBSET'),
	path('archive/WAEC/<str:subject_name>/', view.WAECArchive, name='WAECarchive'),
	
	path('archive/<str:PQ_Type>/', view.ArchiveView, name = 'archive'),
	
	
	path('contact/',view.contact,name='contact'),
	path('register/',view.student_register,name='student_register'),
	path('teacher_register/', view.teacher_register,name='teacher_register'),
	path('Choose_Subjects/', view.Choose_Subjects, name='choose_subject'),
	
	path('results/',view.search,name='search'),

	path('teacher_update/',view.TeacherUpdate, name='teacher_update'),
	path('student_update/',view.StudentUpdate, name='student_update'),
	#path('PastQuestions/',views.PQ,name='PQ'),
	
]