from django.contrib import admin
from accounts.models import *
from django.contrib.auth.admin import UserAdmin
from accounts.models import User
from django.contrib.auth import get_user_model
from django import forms
from import_export.admin import ImportExportModelAdmin

from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _
# Register your models here.

admin.site.site_header='SEF TUTOR Administration'
admin.site.index_title = 'SEF TUTOR Admin'
admin.site.register(Teacher,MyModelAdmin)
admin.site.register(Student,MyModelAdmin)
admin.site.register(Classe)
admin.site.register(Subject)
admin.site.register(Lesson)
admin.site.register(Contact)
admin.site.register(PastQuestion)

@admin.register(Email)
class EmailAdmin(ImportExportModelAdmin):
    pass


@admin.register(User)	
class CustomUserAdmin(UserAdmin,ImportExportModelAdmin):
    list_filter = ['is_staff', 'is_superuser','is_active','is_teacher']    
    list_display = ['username', 'email', 'first_name', 'last_name', 'is_staff','is_active','is_teacher']
    def get_teacher(self,instance):
        return instance.is_teacher
    get_teacher.short_description = 'Teacher Status'
    get_teacher.boolean = True
	
	
class UserCreationFormExtended(UserCreationForm): 
    def __init__(self, *args, **kwargs): 
        super(UserCreationFormExtended, self).__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(label=_("E-mail"), max_length=75)
		
UserAdmin.add_form = UserCreationFormExtended
UserAdmin.change_form = UserCreationFormExtended
UserAdmin.add_fieldsets = (
    (None, {
        'classes': ('wide',),
        'fields': ( 'email','username', 'password1', 'password2','is_teacher')
    }),
)


	

